# OTUS-ELK-ANSIBLE

Этот проект создан для автоматизации установки и конфигурирования Elasticsearch, Logstash, Kibana, Nginx и Filebeat через Ansible на двух виртуальных машинах, основанных на CentOS 8.

## Использование

1. Установите [Vagrant](https://www.vagrantup.com/) и [VirtualBox](https://www.virtualbox.org/).
2. Клонируйте этот репозиторий на свой компьютер.
3. В терминале перейдите в каталог с файлами проекта.
4. Запустите команду `vagrant up` для создания и запуска виртуальных машин.
5. Запустите скрипт `00-preinstall.sh`.
6. Запустите плейбук.

## Конфигурация

### Инвентарь

Файл `inventory.ini` содержит список хостов, которые необходимы для установки Elasticsearch, Logstash, Kibana, Nginx и Filebeat. В текущем проекте есть два хоста, `log` и `web`.

### Плейбук Ansible

Плейбук Ansible для установки и конфигурации Elasticsearch, Logstash и Kibana на хосте `log` находится в файле `install-elk.yml`. Плейбук для установки и настройки Nginx и Filebeat на хосте `web` находится в том же файле.

Первая часть плейбука необходима для установки и настройки Elasticsearch, Logstash и Kibana. Задачи в нем производят установку данных приложений на сервер, задание необходимых настроек и запуск служб.

Ниже представлено описание каждой части данного плейбука:

```
- hosts: log
  tags: log
  roles:
    - geerlingguy.elasticsearch
    - geerlingguy.logstash
    - geerlingguy.kibana
  become: true
  become_method: sudo
```

- `hosts: log` — указывает, что данный плейбук будет выполнен только на хосте `log`, который определен в инвентаре.
- `tags: log` — теги плейбука для выполнения его отдельно или вместе с другими плейбуками.
- `roles:` — список ролей, поставляемых с помощью Ansible Galaxy.
- `geerlingguy.elasticsearch` — роль для установки и настройки Elasticsearch.
- `geerlingguy.logstash` — роль для установки и настройки Logstash.
- `geerlingguy.kibana` — роль для установки и настройки Kibana.
- `become: true` и `become_method: sudo` — с помощью указанных параметров указывается, что для выполнения задач в плейбуке Ansible нужны права суперпользователя.

```
  post_tasks:
  - name: Open Elasticsearch HTTP port
    iptables:
      rule:
        - chain: INPUT
          protocol: tcp
          port: "{{ elasticsearch_http_publish_port }}"
          jump: ACCEPT
      permanent: true
      state: enabled
```

Эта часть является post-task-частью плейбука. Здесь устанавливается правило для открытия порта HTTP для Elasticsearch на сервере. В данном случае используется модуль Ansible `iptables`, который позволяет управлять правилами iptables.

```
  vars:
    kibana_system_password: changeme
```

`kibana_system_password` — переменная для пароля системного пользователя Kibana, устанавливаемого со значением "changeme".

Ссылки на используемые роли `geerlingguy.elasticsearch`, `geerlingguy.logstash` и `geerlingguy.kibana`:

- [geerlingguy.elasticsearch](https://galaxy.ansible.com/geerlingguy/elasticsearch)
- [geerlingguy.logstash](https://galaxy.ansible.com/geerlingguy/logstash)
- [geerlingguy.kibana](https://galaxy.ansible.com/geerlingguy/kibana)

Вторая часть плейбука для установки и настройки Nginx и Filebeat. Эта часть плейбука устанавливает данные приложения на сервер, задает необходимые настройки и запускает службы.

```
- hosts: web
  tags: web
  roles:
    - geerlingguy.nginx
    - geerlingguy.filebeat
  become: true
  become_method: sudo
```

- `hosts: web` — указывает, что данный плейбук будет выполнен только на хосте `web`, который определен в инвентаре.
- `tags: web` — теги плейбука для выполнения его отдельно или вместе с другими плейбуками.
- `roles:` — список ролей, поставляемых с помощью Ansible Galaxy.
- `geerlingguy.nginx` — роль для установки и настройки Nginx.
- `geerlingguy.filebeat` — роль для установки и настройки Filebeat.
- `become: true` и `become_method: sudo` — с помощью указанных параметров указывается, что для выполнения задач в плейбуке Ansible нужны права суперпользователя.

```
  vars:
    filebeat_inputs:
      - type: log
        paths:
          - /var/log/nginx/*.log
        tags: ["nginx"]
      - type: log
        paths:
          - /var/log/messages
        tags: ["syslog"]
    filebeat_output_logstash_enabled: true
    filebeat_output_logstash_hosts:
      - "{{ groups['log'][0] }}:5044"
    filebeat_output_elasticsearch_hosts:
      - "{{ groups['log'][0] }}:9200"
    filebeat_setup_kibana_host: "{{ groups['log'][0] }}"
    filebeat_setup_kibana_protocol: "http"
    filebeat_setup_kibana_username: "elastic"
    filebeat_setup_kibana_password: "{{ hostvars['log']['kibana_system_password'] }}"
```

В этой части устанавливаются переменные, которые используются модулем `geerlingguy.filebeat` для конфигурации Filebeat. Эти переменные определяют следующие параметры:
- `filebeat_inputs` — список источников логов, на которые настроен Filebeat.
- `filebeat_output_logstash_enabled` — параметр, который включает использование Logstash в качестве вывода для Filebeat.
- `filebeat_output_logstash_hosts` — определяет хост и порт для Logstash, который будет использоваться для отправки данных Filebeat.
- `filebeat_output_elasticsearch_hosts` — определяет хост и порт для Elasticsearch, который будет использоваться для отправки данных Filebeat.
- `filebeat_setup_kibana_host` — определяет хост Kibana, который будет использоваться для взаимодействия с ним.
- `filebeat_setup_kibana_protocol` — определяет протокол, который будет использоваться для взаимодействия с Kibana.
- `filebeat_setup_kibana_username` — определяет имя пользователя, используемого для подключения к Kibana.
- `filebeat_setup_kibana_password` — определяет пароль пользователя, используемый для подключения к Kibana.

Ссылки на используемые роли `geerlingguy.nginx` и `geerlingguy.filebeat`:

- [geerlingguy.nginx](https://galaxy.ansible.com/geerlingguy/nginx)
- [geerlingguy.filebeat](https://galaxy.ansible.com/geerlingguy/filebeat)

### Скрипт pre-installation

Файл `00-preinstall.sh` содержит команды для установки всех необходимых ролей Ansible через `ansible-galaxy`, а также для импорта ключей SSH, чтобы Ansible мог использовать их для подключения к серверам.

### Vagrantfile

Файл `Vagrantfile` содержит конфигурацию виртуальных машин, которые будут созданы при запуске `vagrant up`. Он определяет имя, ОС, IP-адрес, кол-во CPU, объем памяти и многое другое.

Этот Vagrantfile используется для создания двух виртуальных машин - log и web. Обе они используют базовый образ "centos/stream8".

ВМ log имеет IP-адрес "192.168.88.172" на публичной сети и настроена с 2048MB памяти и 4 процессорами. Его имя хоста установлено на "log".

ВМ web имеет IP-адрес "192.168.88.171" на публичной сети и настроена с 1024MB памяти и 2 процессорами. Его имя хоста установлено на "web".

Для использования этого Vagrantfile убедитесь, что у вас установлены Vagrant и VirtualBox на вашей машине. Затем просто запустите `vagrant up`, чтобы создать ВМ. Вы можете получить доступ к ВМ с помощью `vagrant ssh <vm-name>`, где `<vm-name>` это "log" или "web".