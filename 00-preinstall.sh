#!/bin/bash

# Install roles
ansible-galaxy install geerlingguy.java
ansible-galaxy install geerlingguy.elasticsearch
ansible-galaxy install geerlingguy.logstash
ansible-galaxy install geerlingguy.kibana
ansible-galaxy install geerlingguy.nginx
ansible-galaxy install geerlingguy.filebeat

# Copy vagrant SSH keys
cp .vagrant/machines/log/virtualbox/private_key ~/.ssh/log
cp .vagrant/machines/web/virtualbox/private_key ~/.ssh/web
chmod 600 ~/.ssh/log
chmod 600 ~/.ssh/web

# Add SSH keys to SSH config
cat <<EOF > ~/.ssh/config

Host 192.168.88.172
    HostName 192.168.88.172
    User root
    IdentityFile ~/.ssh/log

Host 192.168.88.171
    HostName 192.168.88.171
    User root
    IdentityFile ~/.ssh/web
EOF